import pandas as pd
import numpy as np
import json
import os

from sys import platform
import subprocess as subp
from io import StringIO
import requests
import records


def extract_json_values(obj, key):
    """Pull all values of a specified key from a nested JSON."""
    arr = []

    def extract(obj, arr, key):
        """Recursively search for values of key in JSON tree."""
        if isinstance(obj, dict):
            for val1, val2 in obj.items():
                if isinstance(val2, (dict, list)):
                    extract(val2, arr, key)
                elif val1 == key:
                    arr.append(val2)
        elif isinstance(obj, list):
            for item in obj:
                extract(item, arr, key)
        return arr

    results = extract(obj, arr, key)
    return results


def run_shell_cmd(cmd, opt):
    # opt setting should be either 0 or 1
    df = pd.DataFrame()
    cmdtst = cmd

    try:
        process = subp.Popen(cmdtst, stdin=None, stdout=subp.PIPE, stderr=subp.DEVNULL)
        result, err = process.communicate(timeout=5)
        out = StringIO(result.decode('utf-8'))
        code = process.returncode

    except subp.CalledProcessError as e:
        print('ERROR in shell: ' + str(e))

    if code >= 1:
        print('ERROR in shell output:' + code)
    else:
        if opt < 1:
            # this option dose not skip header line or the first row.
            df = pd.read_csv(out, engine='python', header=None, sep=' ')
        else:
            # this option skips the header line or the first row.
            df = pd.read_csv(out, engine='python', header=None, skiprows=1, sep='\\s+')

    return df







def main():

    df_raidPG = pd.DataFrame()
    #for filename in os.listdir(path='/Users/graham/Work/files'):
    #two paths for local and network versions

#curl -k -1 -H 'Authorization: Basic Z2R1bmtsZXk6SCF0NGNoaTE=' 'https://ulpdca001.auiag.corp:8443/dbapi.do?action=query&dataset=defaultDs&processSync=true' -H 'Content-Type: application/json' -d '{"query":"*raidStorage[=name rx .*]/raidPG[=pgType rx .*]&[=displayDriveType rx .*]","startTime":"20190801_000000","endTime":"20190801_000001"}' > /storage/00-Analysts/Graham/data/raidPG.json

    command = "curl -k -1 -H \'Authorization: Basic Z2R1bmtsZXk6SCF0NGNoaTE=\' \'https://ulpdca001.auiag.corp:8443/dbapi.do?action=query&dataset=defaultDs&processSync=true\' -H \'Content-Type: application/json\' -d \'{\"query\":\"*raidStorage[=name rx .*]/raidPG[=pgType rx .*]&[=displayDriveType rx .*]\",\"startTime\":\"20190801_000000\",\"endTime\":\"20190801_000001\"}\' > /storage/00-Analysts/Graham/data/raidPG.json"




    filename = '/Users/graham/Work/files/raidPG.json'
    # filename = '/Volumes/HDS_Storage/00-Analysts/Graham/data/raidPG.json'

    with open(filename, 'r') as file1:
        sigjson = json.load(file1)


    # extracting values for 'signature' from json, then adding to df, requires data munging
    df_sig = pd.DataFrame(extract_json_values(sigjson, 'signature'))
    df_sig = df_sig[0].str.split("-", n=1, expand=True)
    df_sig[0] = df_sig[0].str.extract(r'(\d\d\d\d\d)')
    df_sig.columns = ['Serial', 'RaidPG']
    df_sig.head()

    # extracting values for 'data' from json, then adding to df
    df_dat = pd.DataFrame(extract_json_values(sigjson, 'data'))

    # df_dat.to_csv('/Users/graham/Work/files/dfdat.csv', index=False, encoding='utf-8')

    df_dat.head()

    # Sort 'data' variables into columns
    df_dat = pd.DataFrame(df_dat.values.reshape(-1, 2), columns=['PGtype', 'PGTier'])
    df_dat.head()
    df_all = pd.concat([df_sig, df_dat], axis=1)
    df_raidPG = df_raidPG.append(df_all, ignore_index=True, sort=False)


    df_raidPG['UnitName'] = None
    # df_raidPG['UnitName'][df_raidPG['Serial'].str.contains('53280')] = 'MDCVSP01'
    df_raidPG['UnitName'][df_raidPG['Serial'].str.contains('55914')] = 'MDCVSP02'
    df_raidPG['UnitName'][df_raidPG['Serial'].str.contains('54831')] = 'MDCVSP03'
    df_raidPG['UnitName'][df_raidPG['Serial'].str.contains('50441')] = 'MDCVSP11'
    df_raidPG['UnitName'][df_raidPG['Serial'].str.contains('51971')] = 'MDCVSP12'

    df_raidPG['UnitName'][df_raidPG['Serial'].str.contains('55916')] = 'WDCVSP02'
    df_raidPG['UnitName'][df_raidPG['Serial'].str.contains('55953')] = 'WDCVSP03'
    df_raidPG['UnitName'][df_raidPG['Serial'].str.contains('51206')] = 'WDCVSP11'
    df_raidPG['UnitName'][df_raidPG['Serial'].str.contains('51876')] = 'WDCVSP12'

    df_raidPG.head()
    df_raidPG.info()
    df_raidPG.describe()




    df_pgroup = pd.DataFrame()
    df_pgroup = pd.pivot_table(df_raidPG, values='RaidPG', index=['PGtype', 'UnitName', 'Serial'], columns=['PGTier'], aggfunc=lambda x: len(x.unique()), fill_value=0)
    pd.pivot_table(df_raidPG, values='RaidPG', index=['PGtype', 'UnitName', 'Serial'], columns=['PGTier'], aggfunc=lambda x: len(x.unique()), fill_value=0)


    # df_pgroup.to_csv('/Users/graham/Work/files/raidPG_summary.csv', index=False, encoding='utf-8')





main()
