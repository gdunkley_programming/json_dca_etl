'''
This script extracts deeply embeded jason metrics from the curl output from the Hitachi RMDB in HDCA
a example of the curl used to extract this data is:
curl -k -1 -H 'Authorization: Basic Z2R1bmtsZXk6SCF0NGNoaTE='
'https://ulpdca001.auiag.corp:8443/dbapi.do?action=query&dataset=defaultDs&processSync=true'
 -H 'Content-Type: application/json'
 -d '{"query":"*raidStorage[=name rx .*]/raidPG[=pgType rx .*]&[=displayDriveType rx .*]",
 "startTime":"20190624_000000","endTime":"20190624_000001"}'
 > /storage/00-Analysts/Graham/data/raidPG.json
'''

import pandas as pd
import json
import os


def extract_json_values(obj, key):
    """Pull all values of a specified key from a nested JSON."""
    arr = []

    def extract(obj, arr, key):
        """Recursively search for values of key in JSON tree."""
        if isinstance(obj, dict):
            for val1, val2 in obj.items():
                if isinstance(val2, (dict, list)):
                    extract(val2, arr, key)
                elif val1 == key:
                    arr.append(val2)
        elif isinstance(obj, list):
            for item in obj:
                extract(item, arr, key)
        return arr

    results = extract(obj, arr, key)
    return results


df_raidPG = pd.DataFrame()
for filename in os.listdir(path='/Users/graham/Work/files'):

    # Opens multiple files ending with .json, to extract and combine into one df and csv.
    if filename.endswith('.json'):

        with open(filename, 'r') as file1:
            sigjson = json.load(file1)

        # extracting values for 'signature' from json, then adding to df, requires data mangling
        df_sig = pd.DataFrame(extract_json_values(sigjson, 'signature'))
        df_sig = df_sig[0].str.split("-", n=1, expand=True)
        df_sig[0] = df_sig[0].str.extract(r'(\d\d\d\d\d)')
        df_sig.columns = ['Serial', 'RaidPG']

        # extracting values for 'data' from json, then adding to df
        df_dat = pd.DataFrame(extract_json_values(sigjson, 'data'))
        # Sort 'data' variables into columns
        df_dat = pd.DataFrame(df_dat.values.reshape(-1, 2), columns=['PGtype', 'PGTier'])

        df_all = pd.concat([df_sig, df_dat], axis=1)

        df_raidPG = df_raidPG.append(df_all, ignore_index=True)
        print(df_sig.head())
        print(df_dat)
        print(df_all)


df_raidPG.to_csv('/Users/graham/Work/files/raidPG_summary.csv', index=False, encoding='utf-8')

